usuarios = []


def adicionar_usuario(cpf,nome,email,senha):    
    usuario = [cpf, nome, email, senha]
    usuarios.append(usuario)
    
def listar_usuarios():
    return usuarios

def buscar_usuario(cpf):
    for u in usuarios:
        if (u[0] == cpf):
            return u
    return None
        
def remover_usuario(cpf):
    for u in usuarios:
        if (u[0] == cpf):
            usuarios.remove(u)
            return True
    return False

def remover_todos_usuarios():
    global usuarios
    usuarios = []
    
def iniciar_usuarios():
    adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
    adicionar_usuario(22222222222, "Karollaynne", "karollaynne_funkeira@ig.bol","4321")
    
    
