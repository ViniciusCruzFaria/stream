
filmes = []
codigo_geral = 0

def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral
    
    

def adicionar_filme(titulo,genero,ano):
    codigo = _gerar_codigo()
    
    t = titulo
    g = genero
    a = ano
   
    filme = [codigo,t,g,a]
    filmes.append(filme)
    
def listar_filmes():
    return filmes    

def buscar_filme(cod_filme):
    for t in filmes:
        if (t[0] == cod_filme):
            return t
    return None

def buscar_filme_por_nome(nome):
    for t in filmes:
        if (t[1] == nome):
            return t
    return None

def buscar_filmes_por_genero(genero):
    gen = []
    for t in filmes:
        if (t[2] == genero):
            gen.append(t)
    return gen


def remover_filme(cod_filme):
    for t in filmes:
        if (t[0] == cod_filme):
            filmes.remove(t)
            return True
    return False

def remover_todos_filmes():
    global filmes
    filmes = []

def zerar_codigo():
    global codigo_geral
    codigo_geral = 0
    
    
def iniciar_filmes():
    adicionar_filme("X-Men Apocalipse","Ação","2016")
    adicionar_filme("Split","Ficção Científica","2017")
    adicionar_filme("Up! Altas Aventuras","Animação","2009")
