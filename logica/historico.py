from logica import filme

historicos = []

def registrar_filme_assistido(cod_filme,user):
    i = 0
    for c in historicos:
        if (c[0][0] == user[0]):
            historicos[i][1].append(filme.buscar_filme(cod_filme))
            return True
        i += 1
    titulo = filme.buscar_filme(cod_filme)
    historicos.append([user,[titulo]])
    
def listar_historicos():
    return historicos

def listar_filmes_assistidos(cpf):
    for c in historicos:
        if (c[0][0] == cpf):
            return c[1]
    return None

def remover_historico_por_cpf(cpf):
    for c in historicos:
        if (c[0][0] == cpf):
            historicos.remove(c)
            return True
    return None

def remover_todos_historicos():
    global historicos
    historicos = []

