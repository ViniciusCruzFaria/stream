from logica import filme

sugestoes = []
codigo_geral = 0

def _gerar_codigo():
    global codigo_geral
    codigo_geral += 1
    return codigo_geral

def escrever_sugestao(cpf,filme,genero,ano):
    codigo = _gerar_codigo()
    sugestao = [codigo,cpf,filme,ano,genero]
    sugestoes.append(sugestao)

def listar_sugestao():
    global sugestoes
    return sugestoes

def buscar_sugestao(cod_sugestao):
    for s in sugestoes:
        if (s[0] == cod_sugestao):
            return s
    return None

def remover_sugestao(cod_sugestao):
    for s in sugestoes:
        if (s[0] == cod_sugestao):
            sugestoes.remove(s)
            return True
    return False

def remover_todas_sugestoes():
    global sugestoes
    sugestoes = []

def zerar_codigo():
    global codigo_geral
    codigo_geral = 0

def iniciar_sugestao():
    escrever_sugestao(11111111111,"GG","Ação","2019")
    escrever_sugestao(11111111111,"IZI","Ação","2020")
    escrever_sugestao(11111111111,"Acabou","Ação","2030")
    
