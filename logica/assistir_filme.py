import sys
sys.path.insert(0, "E:\Mackenzie\APDI\Stream")

from logica import filme
from logica import usuario
from logica import historico
import pygame
import imageio
imageio.plugins.ffmpeg.download()
from moviepy.editor import VideoFileClip

def assistir(cpf,nome):
    user = usuario.buscar_usuario(cpf)
    while (usuario == None):
        cpf = int(input("Informe o Cpf: "))
        user = usuario.buscar_usuario(cpf)
    titulo = filme.buscar_filme_por_nome(nome)
    while (titulo == None):
        nome = str(input("Informe o nome do filme: "))
        titulo = filme.buscar_filme_por_nome(nome)
    historico.registrar_filme_assistido(titulo[0],user)
    pygame.display.set_caption('Stream de video!')
    barra ='\\'
    video = 'video'+barra+nome+".avi"
    clip = VideoFileClip(video)
    clip.preview()
    pygame.quit()
    sys.exit()
    return True
