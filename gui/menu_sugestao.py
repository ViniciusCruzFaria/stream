from logica import sugestao
from logica import filme
from logica import usuario
from gui import menu_usuario

def imprimir_sugestao(sugestao):
    codigo = sugestao[0]
    cpf = sugestao[1]
    titulo = sugestao[2]
    genero = sugestao[3]
    ano = sugestao[4]
    
    u = usuario.buscar_usuario(cpf)
    print("Codigo da sugestão: ",codigo)
    print("Usuario que fez a sugestão:")
    menu_usuario.imprimir_usuario(u)
    print("Filme: ",titulo)
    print("Gênero: ",genero)
    print("Ano: ",ano)
    print()

def menu_adicionar():
    print ("\nAdicionar Sugestão  \n")
    cpf = int(input("informe seu CPF: "))
    existe = usuario.buscar_usuario(cpf)
    while (existe == None):
        cpf = int(input("informe seu CPF: "))
        existe = usuario.buscar_usuario(cpf) 
    titulo = str(input("Título do Filme: "))
    existe = filme.buscar_filme_por_nome(titulo)
    while (existe == True):
        titulo = str(input("Título do Filme: "))
        existe = filme.buscar_filme_por_nome(titulo)   
    genero = str(input("Gênero do Filme: "))
    ano = str(input("Ano do Filme: "))
    sugest= sugestao.escrever_sugestao(cpf,titulo,genero,ano)
    print ("Sugestão Adicionada")
    
        

def menu_listar():
    print ("\nListar Sugestões \n")
    sugestoes = sugestao.listar_sugestao()
    for s in sugestoes:
        imprimir_sugestao(s)


def menu_buscar():
    print ("\nBuscar Sugestão \n")
    cod = int(input("Código: "))
    s = sugestao.buscar_sugestao(cod)
    if (s == None):
        print ("Sugestão não encontrada")
    else:
        imprimir_sugestao(s)

def menu_remover():
    print ("\nRemover Sugestão \n")
    codigo = int(input("Codigo Sugestão: "))
    s = sugestao.remover_sugestao(codigo)
    if (s == False):
        print ("Sugestão não encontrado")
    else:
        print ("Sugestão removido")
    

def exibir_menu_sugestao():
    run_sugestao = True
    menu = ("\n----------------\n"+
             "(1) Adicionar Sugestão \n" +
             "(2) Listar Sugestão \n" +
             "(3) Buscar Sugestão por Código \n" +
             "(4) Remover Sugestão \n" +
             "(0) Voltar\n"+
            "----------------")
    
    while(run_sugestao):
        print (menu)
        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_adicionar()
        elif(op == 2):
            menu_listar()
        elif(op == 3):       
            menu_buscar()
        elif (op == 4):
            menu_remover()
        elif (op == 0):
            run_sugestao = False
        else:
            print ("Valor invalido") 
