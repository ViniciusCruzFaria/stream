from logica import filme
from gui import menu_usuario


def imprimir_filme(filme):
    codigo = filme[0]
    titulo = filme[1]
    genero = filme[2]
    ano = filme[3]
    
    print("Codigo do Filme: ",codigo)
    print("Filme: ",titulo)
    print("Gênero: ",genero)
    print("Ano: ",ano)
    print()

def imprimir_filme_genero(filme):
    f = filme
    for i in f:
        codigo = i[0]
        titulo = i[1]
        genero = i[2]
        ano = i[3]
    
        print("Codigo do Filme: ",codigo)
        print("Filme: ",titulo)
        print("Gênero: ",genero)
        print("Ano: ",ano)
        print()
        

def menu_adicionar():
    print ("\nAdicionar Filme  \n")
    titulo = str(input("Título do Filme: "))
    genero = str(input("Gênero do Filme: "))
    ano = str(input("Ano do Filme: "))
    adicionou = filme.adicionar_filme(titulo,genero,ano)
    print ("Filme Adicionado")
    
        

def menu_listar():
    print ("\nListar Filmes \n")
    filmes = filme.listar_filmes()
    for f in filmes:
        imprimir_filme(f)


def menu_buscar():
    print ("\nBuscar Filme \n")
    cod = int(input("Código: "))
    f = filme.buscar_filme(cod)
    if (f == None):
        print ("Filme não encontrado")
    else:
        imprimir_filme(f)

def menu_buscar_por_genero():
    print ("\nBuscar Filme \n")
    genero = str(input("Gênero: "))
    f = filme.buscar_filmes_por_genero(genero)
    if (len(f) == 0):
        print ("Filme não encontrado")
    else:
        imprimir_filme_genero(f)

def menu_remover():
    print ("\nRemover Filme \n")
    codigo = int(input("Codigo Filme: "))
    f = filme.remover_filme(codigo)
    if (f == False):
        print ("Filme não encontrado")
    else:
        print ("Filme removido")
    

def exibir_menu_filme():
    run_filme = True
    menu = ("\n----------------\n"+
             "(1) Adicionar Novo Filme \n" +
             "(2) Listar Filme \n" +
             "(3) Buscar Filme por Código \n" +
             "(4) Buscar Filme por Gênero \n" +
             "(5) Remover Filme \n" +
             "(0) Voltar\n"+
            "----------------")
    
    while(run_filme):
        print (menu)
        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_adicionar()
        elif(op == 2):
            menu_listar()
        elif(op == 3):       
            menu_buscar()
        elif (op == 4):
            menu_buscar_por_genero()
        elif (op == 5):
            menu_remover()
        elif (op == 0):
            run_filme = False
        else:
            print ("Valor invalido")
            
    
