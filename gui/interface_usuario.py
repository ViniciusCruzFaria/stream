from logica import usuario
from gui import  menu_usuario

from logica import filme
from gui import  menu_filme

from gui import menu_assistir
from gui import menu_sugestao
from logica import sugestao

def inicializar_dados():
    usuario.iniciar_usuarios()
    filme.iniciar_filmes()
    sugestao.iniciar_sugestao()
    
def exibir_menu_principal():
    run_menu = True
   
    inicializar_dados()

    
    menu = ("\n----------------\n"+
             "(1) Menu Usuário \n" +
             "(2) Menu Filme \n" +
             "(3) Menu Assistir filme \n" +
             "(4) Menu Sugestões \n" +
             "(0) Sair\n"+
            "----------------")
    
    while(run_menu):
        print (menu)
        
        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_usuario.exibir_menu_usuario()
        elif(op == 2):
            menu_filme.exibir_menu_filme()
        elif(op == 3):
            menu_assistir.exibir_menu_assistir()
        elif(op == 4):
            menu_sugestao.exibir_menu_sugestao()
        elif (op == 0):
            print ("Saindo do programa...")
            run_menu = False
        else:
            print ("Valor invalido")
