from logica import assistir_filme
from logica import historico
from gui import menu_filme

def menu_assistir():
    cpf = int(input("Informe o Cpf: "))
    nome = str(input("Informe o nome do filme: "))
    if (assistir_filme.assistir(cpf,nome)):
        print("Filme adicionado ao historico")
        
def menu_historico():
    cpf = int(input("Informe o Cpf: "))
    hist = historico.listar_filmes_assistidos(cpf)
    print("")
    print("HISTORICO")
    for t in hist:
        menu_filme.imprimir_filme(t)
    
def exibir_menu_assistir():
    run_assistir = True
    menu = ("\n----------------\n"+
             "(1) Assistir Filme \n" +
             "(2) Historico por CPF \n" +
             "(0) Voltar\n"+
            "----------------")
    
    while(run_assistir):
        print (menu)
        op = int(input("Digite sua escolha: "))

        if (op == 1):
            menu_assistir()
        elif (op == 2):
            menu_historico()
        elif (op == 0):
            run_assistir = False
        else:
            print ("Valor invalido")
