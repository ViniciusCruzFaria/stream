import unittest
import sys
sys.path.insert(0, "E:\Mackenzie\APDI\Stream")

from logica import usuario

class TestUsuario(unittest.TestCase):
    
    def setUp(self):
        usuario.remover_todos_usuarios()
        
    def test_sem_pacientes(self):
        usuarios = usuario.listar_usuarios()
        self.assertEqual(0, len(usuarios))
        
    def test_adicionar_um_usuario(self):
        usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")

        usuarios = usuario.listar_usuarios()
        self.assertEqual(1, len(usuarios))

        u = usuarios[0]
        
        self.assertEqual(11111111111, u[0])        
        self.assertEqual("Vynnycius", u[1])
        self.assertEqual("vynny_gatinho@ig.bol", u[2])
        self.assertEqual("1234", u[3])

    def test_adicionar_dois_usuarios(self):
        usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
        usuario.adicionar_usuario(22222222222, "Karollaynne", "karollaynne_funkeira@ig.bol","4321")
        usuarios = usuario.listar_usuarios()
        self.assertEqual(2, len(usuarios))

    def test_buscar_usuario(self):
        usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
        usuario.adicionar_usuario(22222222222, "Karollaynne", "karollaynne_funkeira@ig.bol","4321")

        u = usuario.buscar_usuario(11111111111)
        self.assertEqual(11111111111, u[0])
        self.assertEqual("Vynnycius", u[1])
        
    def test_remover_usuario(self):
        usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
        usuario.adicionar_usuario(22222222222, "Karollaynne", "karollaynne_funkeira@ig.bol","4321")
        
        usuario.remover_usuario(11111111111)
        
        u = usuario.buscar_usuario(11111111111)        
        self.assertIsNone(u)
     
    def test_remover_todos_usuarios(self):
        usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
        usuario.adicionar_usuario(22222222222, "Karollaynne", "karollaynne_funkeira@ig.bol","4321")
        
        usuario.remover_todos_usuarios()
        
        u = usuario.listar_usuarios()
        self.assertEqual([], u)       
        
        
    def test_iniciar_usuarios(self)  :
        usuario.iniciar_usuarios()
        usuarios = usuario.listar_usuarios()
        self.assertEqual(2, len(usuarios))
        
        
            
if __name__ == '__main__':
    unittest.main(exit=False)
