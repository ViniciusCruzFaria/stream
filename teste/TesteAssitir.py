import unittest
import sys
sys.path.insert(0, "E:\Mackenzie\APDI\Stream")

from logica import usuario
from logica import filme
from logica import historico
from logica import assistir_filme

class TestAssistir(unittest.TestCase):
     def setUp(self):
          filme.remover_todos_filmes()
          filme.zerar_codigo()
          usuario.remover_todos_usuarios()
          historico.remover_todos_historicos()

     def test_sem_historicos(self):
          hist= historico.listar_historicos()
          self.assertEqual(0, len(hist))
        
     def test_um_filme_historico(self):
          filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
          usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
          assistir_filme.assistir(11111111111,"X-Men Apocalipse")
          hist = historico.listar_filmes_assistidos(11111111111)
          self.assertEqual(1, len(hist))
     
     def test_dois_filme_historico(self):
          filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
          filme.adicionar_filme("Split","Ficção Científica","2017")
          usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
          assistir_filme.assistir(11111111111,"X-Men Apocalipse")
          assistir_filme.assistir(11111111111,"Split")
          hist = historico.listar_filmes_assistidos(11111111111)
          self.assertEqual(2, len(hist))

     def test_remover_historicos_por_cpf(self):
          filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
          usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
          assistir_filme.assistir(11111111111,"X-Men Apocalipse")
          historico.remover_historico_por_cpf(11111111111)
          hist= historico.listar_historicos()
          self.assertEqual(0, len(hist))

     def test_remover_todos_historicos(self):
          filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
          usuario.adicionar_usuario(11111111111, "Vynnycius", "vynny_gatinho@ig.bol", "1234")
          assistir_filme.assistir(11111111111,"X-Men Apocalipse")
          historico.remover_todos_historicos()
          hist= historico.listar_historicos()
          self.assertEqual(0, len(hist))
          
        
if __name__ == '__main__':
     unittest.main(exit=False)
