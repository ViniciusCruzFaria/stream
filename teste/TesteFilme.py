import unittest
import sys
sys.path.insert(0, "E:\Mackenzie\APDI\Stream")

from logica import filme

class TestFilmes (unittest.TestCase):
    
    def setUp(self):
        filme.remover_todos_filmes()
        filme.zerar_codigo()
        
    def test_sem_filmes(self):
        filmes = filme.listar_filmes()
        self.assertEqual(0, len(filmes))
        
    def test_adicionar_um_filme(self):
        filme1 = filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
        filme1 = filme.listar_filmes()
        self.assertEqual(1,len(filme1))

    def test_adicionar_dois_filmes(self):
        filme1 = filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
        filme1 = filme.adicionar_filme("Split","Ficção Científica","2017")
        filme1 = filme.listar_filmes()
        self.assertEqual(2,len(filme1))
        
    def test_buscar_filme(self):
        filme1 = filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
        filme1 = filme.adicionar_filme("Split","Ficção Científica","2017")
        f = filme.buscar_filme(1)
        self.assertEqual(1, f[0])
        self.assertEqual("X-Men: Apocalipse", f[1])

    def test_buscar_filme_por_nome(self):
        filme1 = filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
        filme1 = filme.adicionar_filme("Split","Ficção Científica","2017")
        f = filme.buscar_filme_por_nome("Split")
        self.assertEqual("Split", f[1])

    def test_buscar_filmes_por_genero(self):
        filme1 = filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
        filme1 = filme.adicionar_filme("Split","Ação","2017")
        f = filme.buscar_filmes_por_genero("Ação")
        self.assertEqual(2,len(f))
        
    def test_remover_filme(self):
        filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
        filme.adicionar_filme("Split","Ficção Científica","2017")
        filme.remover_filme(1)
        f = filme.buscar_filme(1)       
        self.assertIsNone(f)

    def test_remover_todos_filmes(self):
        filme.adicionar_filme("X-Men Apocalipse","Ação","2016")
        filme.adicionar_filme("Split","Ficção Científica","2017")
        filme.remover_todos_filmes()
        f = filme.listar_filmes()
        self.assertEqual([], f)
      
    def test_iniciar_filmes(self):
        filme.iniciar_filmes()
        f = filme.listar_filmes()
        self.assertEqual(3,len(f))
    
if __name__ == '__main__':
    unittest.main(exit=False)
