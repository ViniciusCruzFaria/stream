import unittest
import sys
sys.path.insert(0, "E:\Mackenzie\APDI\TRAB")

from logica import sugestao

class TestSugestao(unittest.TestCase):
    
    def setUp(self):
        sugestao.remover_todas_sugestoes()
        sugestao.zerar_codigo()
        
    def test_sem_sugestoes(self):
        sugestoes = sugestao.listar_sugestao()
        self.assertEqual(0, len(sugestoes))
        
    def test_adicionar_uma_sugestao(self):
        sugestao.escrever_sugestao(11111111111,"GG","Ação","2019")

        sugestoes = sugestao.listar_sugestao()
        self.assertEqual(1, len(sugestoes))

    def test_escrever_duas_sugestoes(self):
        sugestao.escrever_sugestao(11111111111,"GG","Ação","2019")
        sugestao.escrever_sugestao(11111111111,"IZI","Ação","2020")
        sugestoes = sugestao.listar_sugestao()
        self.assertEqual(2, len(sugestoes))

    def test_buscar_sugestao(self):
        sugestao.escrever_sugestao(11111111111,"GG","Ação","2019")
        sugestao.escrever_sugestao(11111111111,"IZI","Ação","2020")

        s = sugestao.buscar_sugestao(1)
        self.assertEqual(1, s[0])
        self.assertEqual("GG", s[2])
        
    def test_remover_sugestao(self):
        sugestao.escrever_sugestao(11111111111,"GG","Ação","2019")
        sugestao.escrever_sugestao(11111111111,"IZI","Ação","2020")
        
        sugestao.remover_sugestao(1)
        
        s = sugestao.buscar_sugestao(1)        
        self.assertIsNone(s)
     
    def test_remover_todas_sugestoes(self):
        sugestao.escrever_sugestao(11111111111,"GG","Ação","2019")
        sugestao.escrever_sugestao(11111111111,"IZI","Ação","2020")
        
        sugestao.remover_todas_sugestoes()
        
        s = sugestao.listar_sugestao()
        self.assertEqual([], s)       
        
        
    def test_iniciar_sugestoes(self)  :
        sugestao.iniciar_sugestao()
        sugestoes = sugestao.listar_sugestao()
        self.assertEqual(3, len(sugestoes))
        
        
            
if __name__ == '__main__':
    unittest.main(exit=False)
